LOCAL_PATH := $(call my-dir)

###############################################################################
# Launcher3NoQsb build rule (No QSB, No -1 Screen, Android Q Reference Nav)
#
include $(CLEAR_VARS)

# Relative path for Launcher3 directory
LAUNCHER_PATH := ../../../../packages/apps/Launcher3

LOCAL_STATIC_ANDROID_LIBRARIES := \
    Launcher3CommonDepsLib \
    SecondaryDisplayLauncherLib

ifneq (,$(wildcard frameworks/base))
  LOCAL_STATIC_JAVA_LIBRARIES += SystemUISharedLib launcherprotosnano
  LOCAL_PRIVATE_PLATFORM_APIS := true
else
  LOCAL_STATIC_JAVA_LIBRARIES += libSharedSystemUI libLauncherProtos
  LOCAL_SDK_VERSION := system_current
  LOCAL_MIN_SDK_VERSION := 28
endif

LOCAL_SRC_FILES := \
    $(call all-java-files-under, src_flags) \
    $(call all-java-files-under, $(LAUNCHER_PATH)/quickstep/src) \
    $(call all-java-files-under, $(LAUNCHER_PATH)/src_shortcuts_overrides) \
    $(call all-java-files-under, $(LAUNCHER_PATH)/src) \
    $(call all-java-files-under, $(LAUNCHER_PATH)/quickstep/recents_ui_overrides/src)

LOCAL_RESOURCE_DIR := \
    $(LOCAL_PATH)/$(LAUNCHER_PATH)/quickstep/res \
    $(LOCAL_PATH)/$(LAUNCHER_PATH)/quickstep/recents_ui_overrides/res

LOCAL_PROGUARD_ENABLED := disabled

LOCAL_MODULE_TAGS := optional
LOCAL_USE_AAPT2 := true
LOCAL_AAPT2_ONLY := true
LOCAL_PACKAGE_NAME := Launcher3NoQsb
LOCAL_PRODUCT_MODULE := true
LOCAL_PRIVILEGED_MODULE := true
LOCAL_OVERRIDES_PACKAGES := Home Launcher2 Launcher3 Launcher3QuickStep SearchLauncherQRef MtkLauncher3 MtkLauncher3QuickStep Op09Launcher3 Launcher3Go SearchLauncher SearchLauncherQuickStep MtkLauncher3Go MtkLauncher3GoIconRecents
LOCAL_REQUIRED_MODULES := privapp_whitelist_com.android.launcher3 SearchLauncherConfigOverlay
LOCAL_JACK_COVERAGE_INCLUDE_FILTER := com.android.launcher3.*

LOCAL_FULL_LIBS_MANIFEST_FILES := $(LOCAL_PATH)/$(LAUNCHER_PATH)/quickstep/AndroidManifest.xml

include $(BUILD_PACKAGE)

###############################################################################
# Launcher3GoNoQsb build rule (No QSB, No -1 Screen, Thee-button Nav)
#
include $(CLEAR_VARS)

# Relative path for Launcher3 directory
LAUNCHER_PATH := ../../../../packages/apps/Launcher3

LOCAL_USE_AAPT2 := true
LOCAL_MODULE_TAGS := optional
LOCAL_STATIC_ANDROID_LIBRARIES := Launcher3CommonDepsLib

LOCAL_SRC_FILES := \
    $(call all-java-files-under, $(LAUNCHER_PATH)/src) \
    $(call all-java-files-under, $(LAUNCHER_PATH)/src_ui_overrides) \
    $(call all-java-files-under, go/src)

LOCAL_RESOURCE_DIR := $(LOCAL_PATH)/$(LAUNCHER_PATH)/go/res

LOCAL_PROGUARD_FLAG_FILES := $(LAUNCHER_PATH)/proguard.flags

LOCAL_SDK_VERSION := current
LOCAL_MIN_SDK_VERSION := 21
LOCAL_PACKAGE_NAME := Launcher3GoNoQsb
LOCAL_PRIVILEGED_MODULE := true
LOCAL_PRODUCT_MODULE := true
LOCAL_OVERRIDES_PACKAGES := Home Launcher2 Launcher3 Launcher3QuickStep Launcher3Go MtkLauncher3 MtkLauncher3QuickStep Op09Launcher3 SearchLauncher SearchLauncherQuickStep MtkLauncher3Go MtkLauncher3GoIconRecents
LOCAL_REQUIRED_MODULES := privapp_whitelist_com.android.launcher3

LOCAL_FULL_LIBS_MANIFEST_FILES := \
    $(LOCAL_PATH)/$(LAUNCHER_PATH)/AndroidManifest.xml \
    $(LOCAL_PATH)/$(LAUNCHER_PATH)/AndroidManifest-common.xml

LOCAL_MANIFEST_FILE := $(LAUNCHER_PATH)/go/AndroidManifest.xml
LOCAL_JACK_COVERAGE_INCLUDE_FILTER := com.android.launcher3.*

include $(BUILD_PACKAGE)

###############################################################################
include $(call all-makefiles-under,$(LOCAL_PATH))
