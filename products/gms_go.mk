###############################################################################
# GMS home folder location
# Note: we cannot use $(my-dir) in this makefile
ANDROID_PARTNER_GMS_HOME := vendor/google

$(call inherit-product, build/target/product/product_launched_with_p.mk)
# GMS mandatory core packages
GMS_PRODUCT_PACKAGES += \
    AndroidPlatformServices \
    ConfigUpdater \
    GoogleExtShared \
    GoogleFeedback \
    GoogleLocationHistory \
    GoogleOneTimeInitializer \
    GooglePackageInstaller \
    GooglePartnerSetup \
    GooglePrintRecommendationService \
    GoogleRestore \
    GoogleServicesFramework \
    GoogleSpeechServices \
    GoogleCalendarSyncAdapter \
    GoogleContactsSyncAdapter \
    GoogleTTS \
    GmsCoreGo \
    Phonesky \
    SetupWizard \
    WebViewGoogle \
    Wellbeing

# GMS mandatory libraries
PRODUCT_PACKAGES += com.google.android.maps.jar

# GMS common RRO packages
GMS_PRODUCT_PACKAGES += GmsConfigOverlay GmsGoConfigOverlay GmsGoogleDocumentsUIOverlay

# GMS common configuration files
GMS_PRODUCT_PACKAGES += \
    default_permissions_whitelist_google \
    privapp_permissions_google_system \
    privapp_permissions_google \
    split_permissions_google \
    preferred_apps_google_go \
    sysconfig_wellbeing \
    google_hiddenapi_package_whitelist \
    sysconfig_gmsexpress

# Workaround for b/138542583
PRODUCT_COPY_FILES += $(ANDROID_PARTNER_GMS_HOME)/etc/sysconfig/google_go.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/google.xml

PRODUCT_PACKAGE_OVERLAYS += $(ANDROID_PARTNER_GMS_HOME)/products/go_overlay

# Overlay for GMS devices: default backup transport in SettingsProvider
PRODUCT_PACKAGE_OVERLAYS += $(ANDROID_PARTNER_GMS_HOME)/products/gms_overlay

# Overlay for GoogleDialerGo
ifneq ($(strip $(MTK_TB_WIFI_3G_MODE)),WIFI_ONLY)
ifneq (0x20000000,$(strip $(CUSTOM_CONFIG_MAX_DRAM_SIZE)))
# PRODUCT_PACKAGE_OVERLAYS += $(ANDROID_PARTNER_GMS_HOME)/apps_go/GoogleDialerGo/overlay
GMS_PRODUCT_PACKAGES += GmsDialerGoConfigOverlay GmsDialerGoTelecomOverlay
endif
endif

# GMS mandatory application packages
GMS_PRODUCT_PACKAGES += \
    AssistantGo \
    Chrome \
    GalleryGo \
    GMailGo \
    GoogleSearchGo \
    LatinImeGoogleGo \
    MapsGo \
    NavGo \
    YouTubeGo

# GMS comms suite
GMS_PRODUCT_PACKAGES += \
    CarrierServices

ifneq ($(strip $(MTK_TB_WIFI_3G_MODE)),WIFI_ONLY)
GMS_PRODUCT_PACKAGES += \
    MessagesGo
ifneq (0x20000000,$(strip $(CUSTOM_CONFIG_MAX_DRAM_SIZE)))
GMS_PRODUCT_PACKAGES += \
    GoogleDialerGo
endif
endif

# GMS optional application packages
# 512M project doesn't pre-install some application packages
ifneq (0x20000000,$(strip $(CUSTOM_CONFIG_MAX_DRAM_SIZE)))
GMS_PRODUCT_PACKAGES += \
    CalendarGoogle \
    GoogleContacts \
    DuoGo \
    FilesGoogle
endif

# GMS for home screen layout
ifneq (0x20000000,$(strip $(CUSTOM_CONFIG_MAX_DRAM_SIZE)))
GMS_PRODUCT_PACKAGES += GmsSampleIntegrationGo
else
GMS_PRODUCT_PACKAGES += GmsSampleIntegrationGo512M
endif

# Add GMS package into system product packages
PRODUCT_PACKAGES += $(GMS_PRODUCT_PACKAGES)

PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
    ro.boot.vendor.overlay.theme=com.android.internal.systemui.navbar.threebutton

# Add acsa property for CarrierServices
PRODUCT_PRODUCT_PROPERTIES += \
    ro.com.google.acsa=true

PRODUCT_PRODUCT_PROPERTIES += \
    ro.setupwizard.rotation_locked=true \
    setupwizard.theme=glif_v3_light \
    ro.opa.eligible_device=true \
    ro.com.google.gmsversion=10_202003.go
