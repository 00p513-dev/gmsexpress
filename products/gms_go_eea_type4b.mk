$(call inherit-product, vendor/google/products/eea_go_common.mk)

GMS_PRODUCT_PACKAGES += \
    sysconfig_eea_v1_search \
    AssistantGo \
    GoogleSearchGo \
    Chrome \
    GmsEEAType4bIntegrationGo \
    Launcher3Go

# Add GMS package into system product packages
PRODUCT_PACKAGES += $(GMS_PRODUCT_PACKAGES)