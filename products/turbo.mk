###############################################################################
# Please add "$(call inherit-product, vendor/partner_gms/products/turbo.mk)"
# to your product makefile to integrate Turbo app.
# Note: Turbo currently supports arm64 ABI only.
#
GMS_PRODUCT_PACKAGES += Turbo sysconfig_turbo

# Add GMS package into system product packages
PRODUCT_PACKAGES += $(GMS_PRODUCT_PACKAGES)

# Overlay for Turbo
PRODUCT_PACKAGE_OVERLAYS += vendor/google/apps/Turbo/turbo_overlay
