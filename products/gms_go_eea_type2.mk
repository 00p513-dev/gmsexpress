$(call inherit-product, vendor/google/products/eea_go_common.mk)

GMS_PRODUCT_PACKAGES += \
    sysconfig_eea_v1 \
    Chrome \
    GoogleActionsService \
    GmsEEAType2IntegrationGo \
    Launcher3GoNoQsb

# Add GMS package into system product packages
PRODUCT_PACKAGES += $(GMS_PRODUCT_PACKAGES)