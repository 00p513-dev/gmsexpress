$(call inherit-product, vendor/google/products/eea_common.mk)

GMS_PRODUCT_PACKAGES += \
    sysconfig_eea_v1_search \
    Velvet \
    GmsEEAType3bIntegration \
    SearchLauncherQRef \
    AssistantShell

# Add GMS package into system product packages
PRODUCT_PACKAGES += $(GMS_PRODUCT_PACKAGES)