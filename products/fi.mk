###############################################################################
# Only use this makefile for the Project Fi devices
$(call inherit-product, vendor/google/products/gms.mk)

# GCS and Tycho apps are mandatory for Project Fi
PRODUCT_PACKAGES += GCS Tycho GmsFiConfigOverlay

# Configuration files for Fi apps
PRODUCT_COPY_FILES += \
    vendor/google/etc/sysconfig/google_fi.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/google_fi.xml
