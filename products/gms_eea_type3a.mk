$(call inherit-product, vendor/google/products/eea_common.mk)

GMS_PRODUCT_PACKAGES += \
    sysconfig_eea_v1 \
    Velvet \
    GmsEEAType3aIntegration \
    Launcher3NoQsb \
    AssistantShell

# Add GMS package into system product packages
PRODUCT_PACKAGES += $(GMS_PRODUCT_PACKAGES)