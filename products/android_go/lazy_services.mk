# Use lazy HAL implementations
PRODUCT_PACKAGES += \
    android.hardware.cas@1.1-service-lazy \
    android.hardware.drm@1.0-service-lazy \
    android.hardware.drm@1.2-service-lazy.clearkey \
    android.hardware.light@2.0-service-lazy \
    android.hardware.wifi@1.0-service-lazy \

# Only Pixel 2 and newer devices support lazy widevine HAL and 64 bit camera HAL
PRODUCT_PACKAGES += \
    android.hardware.camera.provider@2.4-service-lazy_64 \
    android.hardware.drm@1.2-service-lazy.widevine

PRODUCT_PROPERTY_OVERRIDES += ro.camera.enableLazyHal=true
