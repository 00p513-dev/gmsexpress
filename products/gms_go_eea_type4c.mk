$(call inherit-product, vendor/google/products/eea_go_common.mk)

GMS_PRODUCT_PACKAGES += \
    sysconfig_eea_v1_search_chrome \
    AssistantGo \
    GoogleSearchGo \
    Chrome \
    GmsEEAType4cIntegrationGo \
    Launcher3Go

# Add GMS package into system product packages
PRODUCT_PACKAGES += $(GMS_PRODUCT_PACKAGES)