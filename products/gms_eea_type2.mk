$(call inherit-product, vendor/google/products/eea_common.mk)

GMS_PRODUCT_PACKAGES += \
    sysconfig_eea_v1 \
    Chrome \
    GoogleSpeechServices \
    GoogleActionsService \
    GmsEEAType2Integration \
    Launcher3NoQsb

# Add GMS package into system product packages
PRODUCT_PACKAGES += $(GMS_PRODUCT_PACKAGES)